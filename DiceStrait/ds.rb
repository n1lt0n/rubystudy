

=begin
https://code.google.com/codejam/contest/6314486/dashboard

Problem

You have a special set of N six-sided dice, each of which has six different positive integers on its faces. Different dice may have different numberings.

You want to arrange some or all of the dice in a row such that the faces on top form a straight (that is, they show consecutive integers). For each die, you can choose which face is on top.

How long is the longest straight that can be formed in this way?

Input

The first line of the input gives the number of test cases, T. T test cases follow. Each test case begins with one line with N, the number of dice. Then, N more lines follow; each of them has six positive integers Dij. The j-th number on the i-th of these lines gives the number on the j-th face of the i-th die.

Output

For each test case, output one line containing Case #x: y, where x is the test case number (starting from 1) and y is the length of the longest straight that can be formed.

=end
